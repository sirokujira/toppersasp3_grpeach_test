/*
 * This file was automatically generated by tecsgen.
 * This file is not intended to be edited.
 */
#ifndef tKernel_TECSGEN_H
#define tKernel_TECSGEN_H

/*
 * celltype          :  tKernel
 * global name       :  tKernel
 * idx_is_id(actual) :  no(no)
 * singleton         :  yes
 * has_CB            :  false
 * has_INIB          :  0
 * rom               :  yes
 * CB initializer    :  yes
 */

/* global header #_IGH_# */
#include "global_tecsgen.h"

/* signature header #_ISH_# */
#include "sKernel_tecsgen.h"
#include "siKernel_tecsgen.h"

#ifndef TOPPERS_MACRO_ONLY

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/* cell INIB type definition #_CIP_# */
typedef const struct tag_tKernel_INIB {
    /* call port #_NEP_# */ 
}  tKernel_INIB;

/* CB not exist. CB corresponding to INIB #_DCI_# */
#define tKernel_CB_tab           tKernel_INIB_tab
#define tKernel_SINGLE_CELL_CB   tKernel_SINGLE_CELL_INIB
#define tKernel_CB               tKernel_INIB
#define tag_tKernel_CB           tag_tKernel_INIB

/* singleton cell CB prototype declaration #_SCP_# */
extern  tKernel_INIB  tKernel_SINGLE_CELL_INIB;


/* celltype IDX type #_CTIX_# */
typedef const struct tag_tKernel_INIB *tKernel_IDX;

/* prototype declaration of entry port function #_EPP_# */
/* sKernel */
Inline ER           tKernel_eKernel_getExtendedInformation( intptr_t* p_exinf);
Inline ER           tKernel_eKernel_sleep();
Inline ER           tKernel_eKernel_sleepTimeout( TMO timeout);
Inline ER           tKernel_eKernel_delay( RELTIM delayTime);
Inline ER           tKernel_eKernel_exit();
Inline ER           tKernel_eKernel_disableTerminate();
Inline ER           tKernel_eKernel_enableTerminate();
Inline bool_t       tKernel_eKernel_senseTerminate();
Inline ER           tKernel_eKernel_setTime( SYSTIM systemTime);
Inline ER           tKernel_eKernel_getTime( SYSTIM* p_systemTime);
Inline ER           tKernel_eKernel_adjustTime( int32_t adjustTime);
Inline HRTCNT       tKernel_eKernel_fetchHighResolutionTimer();
Inline ER           tKernel_eKernel_rotateReadyQueue( PRI taskPriority);
Inline ER           tKernel_eKernel_getTaskId( ID* p_taskId);
Inline ER           tKernel_eKernel_getLoad( PRI taskPriority, uint_t* p_load);
Inline ER           tKernel_eKernel_getNthTask( PRI taskPriority, uint_t nth, ID* p_taskID);
Inline ER           tKernel_eKernel_lockCpu();
Inline ER           tKernel_eKernel_unlockCpu();
Inline ER           tKernel_eKernel_disableDispatch();
Inline ER           tKernel_eKernel_enableDispatch();
Inline bool_t       tKernel_eKernel_senseContext();
Inline bool_t       tKernel_eKernel_senseLock();
Inline bool_t       tKernel_eKernel_senseDispatch();
Inline bool_t       tKernel_eKernel_senseDispatchPendingState();
Inline bool_t       tKernel_eKernel_senseKernel();
Inline ER           tKernel_eKernel_exitKernel();
Inline ER           tKernel_eKernel_changeInterruptPriorityMask( PRI interruptPriority);
Inline ER           tKernel_eKernel_getInterruptPriorityMask( PRI* p_interruptPriority);
/* siKernel */
Inline HRTCNT       tKernel_eiKernel_fetchHighResolutionTimer();
Inline ER           tKernel_eiKernel_rotateReadyQueue( PRI taskPriority);
Inline ER           tKernel_eiKernel_getTaskId( ID* p_taskId);
Inline ER           tKernel_eiKernel_lockCpu();
Inline ER           tKernel_eiKernel_unlockCpu();
Inline bool_t       tKernel_eiKernel_senseContext();
Inline bool_t       tKernel_eiKernel_senseLock();
Inline bool_t       tKernel_eiKernel_senseDispatch();
Inline bool_t       tKernel_eiKernel_senseDispatchPendingState();
Inline bool_t       tKernel_eiKernel_senseKernel();
Inline ER           tKernel_eiKernel_exitKernel();
Inline bool_t       tKernel_eiKernel_exceptionSenseDispatchPendingState( const void* p_exceptionInformation);
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* TOPPERS_MACRO_ONLY */


/* celll CB macro #_GCB_# */
#define tKernel_GET_CELLCB(idx) ((void *)0)
#ifndef TOPPERS_MACRO_ONLY

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* prototype declaration of entry port function (referenced when VMT useless optimise enabled) #_EPSP_# */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* TOPPERS_MACRO_ONLY */


/* cell CB macro (abbrev) #_GCBA_# */
#define GET_CELLCB(idx)  tKernel_GET_CELLCB(idx)

/* CELLCB type (abbrev) #_CCT_# */
#define CELLCB	tKernel_CB

/* celltype IDX type (abbrev) #_CTIXA_# */
#define CELLIDX	tKernel_IDX




/* entry port function macro (abbrev) #_EPM_# */
#define eKernel_getExtendedInformation tKernel_eKernel_getExtendedInformation
#define eKernel_sleep    tKernel_eKernel_sleep
#define eKernel_sleepTimeout tKernel_eKernel_sleepTimeout
#define eKernel_delay    tKernel_eKernel_delay
#define eKernel_exit     tKernel_eKernel_exit
#define eKernel_disableTerminate tKernel_eKernel_disableTerminate
#define eKernel_enableTerminate tKernel_eKernel_enableTerminate
#define eKernel_senseTerminate tKernel_eKernel_senseTerminate
#define eKernel_setTime  tKernel_eKernel_setTime
#define eKernel_getTime  tKernel_eKernel_getTime
#define eKernel_adjustTime tKernel_eKernel_adjustTime
#define eKernel_fetchHighResolutionTimer tKernel_eKernel_fetchHighResolutionTimer
#define eKernel_rotateReadyQueue tKernel_eKernel_rotateReadyQueue
#define eKernel_getTaskId tKernel_eKernel_getTaskId
#define eKernel_getLoad  tKernel_eKernel_getLoad
#define eKernel_getNthTask tKernel_eKernel_getNthTask
#define eKernel_lockCpu  tKernel_eKernel_lockCpu
#define eKernel_unlockCpu tKernel_eKernel_unlockCpu
#define eKernel_disableDispatch tKernel_eKernel_disableDispatch
#define eKernel_enableDispatch tKernel_eKernel_enableDispatch
#define eKernel_senseContext tKernel_eKernel_senseContext
#define eKernel_senseLock tKernel_eKernel_senseLock
#define eKernel_senseDispatch tKernel_eKernel_senseDispatch
#define eKernel_senseDispatchPendingState tKernel_eKernel_senseDispatchPendingState
#define eKernel_senseKernel tKernel_eKernel_senseKernel
#define eKernel_exitKernel tKernel_eKernel_exitKernel
#define eKernel_changeInterruptPriorityMask tKernel_eKernel_changeInterruptPriorityMask
#define eKernel_getInterruptPriorityMask tKernel_eKernel_getInterruptPriorityMask
#define eiKernel_fetchHighResolutionTimer tKernel_eiKernel_fetchHighResolutionTimer
#define eiKernel_rotateReadyQueue tKernel_eiKernel_rotateReadyQueue
#define eiKernel_getTaskId tKernel_eiKernel_getTaskId
#define eiKernel_lockCpu tKernel_eiKernel_lockCpu
#define eiKernel_unlockCpu tKernel_eiKernel_unlockCpu
#define eiKernel_senseContext tKernel_eiKernel_senseContext
#define eiKernel_senseLock tKernel_eiKernel_senseLock
#define eiKernel_senseDispatch tKernel_eiKernel_senseDispatch
#define eiKernel_senseDispatchPendingState tKernel_eiKernel_senseDispatchPendingState
#define eiKernel_senseKernel tKernel_eiKernel_senseKernel
#define eiKernel_exitKernel tKernel_eiKernel_exitKernel
#define eiKernel_exceptionSenseDispatchPendingState tKernel_eiKernel_exceptionSenseDispatchPendingState

/* CB initialize macro #_CIM_# */
#define INITIALIZE_CB()
#define SET_CB_INIB_POINTER(i,p_that)\
	/* empty */
#ifndef TOPPERS_MACRO_ONLY

/*  include inline header #_INL_# */
#include "tKernel_inline.h"

#endif /* TOPPERS_MACRO_ONLY */

#ifdef TOPPERS_CB_TYPE_ONLY

/* undef for inline #_UDF_# */
#undef VALID_IDX
#undef GET_CELLCB
#undef CELLCB
#undef CELLIDX
#undef tKernel_IDX
#undef FOREACH_CELL
#undef END_FOREACH_CELL
#undef INITIALIZE_CB
#undef SET_CB_INIB_POINTER
#undef eKernel_getExtendedInformation
#undef eKernel_sleep
#undef eKernel_sleepTimeout
#undef eKernel_delay
#undef eKernel_exit
#undef eKernel_disableTerminate
#undef eKernel_enableTerminate
#undef eKernel_senseTerminate
#undef eKernel_setTime
#undef eKernel_getTime
#undef eKernel_adjustTime
#undef eKernel_fetchHighResolutionTimer
#undef eKernel_rotateReadyQueue
#undef eKernel_getTaskId
#undef eKernel_getLoad
#undef eKernel_getNthTask
#undef eKernel_lockCpu
#undef eKernel_unlockCpu
#undef eKernel_disableDispatch
#undef eKernel_enableDispatch
#undef eKernel_senseContext
#undef eKernel_senseLock
#undef eKernel_senseDispatch
#undef eKernel_senseDispatchPendingState
#undef eKernel_senseKernel
#undef eKernel_exitKernel
#undef eKernel_changeInterruptPriorityMask
#undef eKernel_getInterruptPriorityMask
#undef eiKernel_fetchHighResolutionTimer
#undef eiKernel_rotateReadyQueue
#undef eiKernel_getTaskId
#undef eiKernel_lockCpu
#undef eiKernel_unlockCpu
#undef eiKernel_senseContext
#undef eiKernel_senseLock
#undef eiKernel_senseDispatch
#undef eiKernel_senseDispatchPendingState
#undef eiKernel_senseKernel
#undef eiKernel_exitKernel
#undef eiKernel_exceptionSenseDispatchPendingState
#endif /* TOPPERS_CB_TYPE_ONLY */

#endif /* tKernel_TECSGENH */
