/*
 * This file was automatically generated by tecsgen.
 * This file is not intended to be edited.
 */
#ifndef nMruby_sMrubyVM_TECSGEN_H
#define nMruby_sMrubyVM_TECSGEN_H

/*
 * signature   :  sMrubyVM
 * global name :  nMruby_sMrubyVM
 * context     :  task
 */

#ifndef TOPPERS_MACRO_ONLY

/* signature descriptor #_SD_# */
struct tag_nMruby_sMrubyVM_VDES {
    struct tag_nMruby_sMrubyVM_VMT *VMT;
};

/* signature function table #_SFT_# */
struct tag_nMruby_sMrubyVM_VMT {
    bool_t         (*initialize__T)( const struct tag_nMruby_sMrubyVM_VDES *edp );
    bool_t         (*run__T)( const struct tag_nMruby_sMrubyVM_VDES *edp );
    void           (*funcall__T)( const struct tag_nMruby_sMrubyVM_VDES *edp, const char_t* name );
    void           (*finalize__T)( const struct tag_nMruby_sMrubyVM_VDES *edp );
};

/* signature descriptor #_SDES_# for dynamic join */
#ifndef Descriptor_of_nMruby_sMrubyVM_Defined
#define  Descriptor_of_nMruby_sMrubyVM_Defined
typedef struct { struct tag_nMruby_sMrubyVM_VDES *vdes; } Descriptor( nMruby_sMrubyVM );
#endif
#endif /* TOPPERS_MACRO_ONLY */

/* function id */
#define	FUNCID_NMRUBY_SMRUBYVM_INITIALIZE      (1)
#define	FUNCID_NMRUBY_SMRUBYVM_RUN             (2)
#define	FUNCID_NMRUBY_SMRUBYVM_FUNCALL         (3)
#define	FUNCID_NMRUBY_SMRUBYVM_FINALIZE        (4)

#endif /* nMruby_sMrubyVM_TECSGEN_H */
